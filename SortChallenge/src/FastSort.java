import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.CharBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.EnumSet;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

public class FastSort {
    public static final String charEncoding = System.getProperty("file.encoding"); //<- This one raised all kinds of hell :(
    public static final Charset charSet = Charset.forName(charEncoding);
    public static long newLineBytes;

    public static void cleanup(File[] fileArray) {
        for (File file : fileArray) {
            if (file.exists()) {
                file.delete();
            }
        }
    }

    public static void makeNewFile(File file, int numberOfEntries) {
        // Write to file
        FileOutputStream fout = null;
        try {
            fout = new FileOutputStream(file.getAbsolutePath());
            Random rng = new Random();
            for (int i = 0; i < numberOfEntries; ++i) {
                fout.write(((Integer) rng.nextInt(1000000000)).toString().getBytes());
                fout.write("\n".getBytes());
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            try {
                fout.close();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void runThread(File in, File out) {
        try {
            //            long start = System.currentTimeMillis();
            FileInputStream inStream = new FileInputStream(in);
            FileChannel fileChannel = inStream.getChannel();
            newLineBytes = "\n".getBytes(charEncoding).length;
            long size = fileChannel.size();

            // Collect inputs
            MappedByteBuffer ioBuffer = fileChannel.map(FileChannel.MapMode.READ_ONLY, 0, size);
            inStream.close();
            //            long end = System.currentTimeMillis();
            //            System.out.println("Process Time: " + (end - start) + " milliseconds");

            // Convert to usable values
            //            start = System.currentTimeMillis();
            //            HashMap<String, Integer> counts = new HashMap<String, Integer>();
            //            TreeSet<String> strs = new TreeSet<String>((String s1, String s2) -> {
            //                return s1.length() == s2.length() ? s1.compareTo(s2) : s1.length() - s2.length();
            //            });
            TreeMap<String, Integer> inputs = new TreeMap<String, Integer>((String s1, String s2) -> {
                return s1.length() == s2.length() ? s1.compareTo(s2) : s1.length() - s2.length();
            });
            StringBuilder sb = new StringBuilder();
            String currStr;
            String input = charSet.decode(ioBuffer).toString();

            for (int i = 0; i < input.length(); ++i) {
                sb.append(input.charAt(i));
                if (input.charAt(i) == '\n') {
                    currStr = sb.toString();
                    if (inputs.containsKey(currStr))
                        inputs.put(currStr, inputs.get(currStr) + 1);
                    else {
                        inputs.put(currStr, 1);
                    }
                    sb.setLength(0);
                }
            }

            //            for (int i = 0; i < input.length(); ++i) {
            //                sb.append(input.charAt(i));
            //                if (input.charAt(i) == '\n') {
            //                    currStr = sb.toString();
            //                    if (counts.containsKey(currStr))
            //                        counts.put(currStr, counts.get(currStr) + 1);
            //                    else {
            //                        counts.put(currStr, 1);
            //                        strs.add(currStr);
            //                    }
            //                    sb.setLength(0);
            //                }
            //            }
            //            end = System.currentTimeMillis();
            //            System.out.println("Process Time: " + (end - start) + " milliseconds");

            // Write output
            //            start = System.currentTimeMillis();
            out.createNewFile();

            ioBuffer = ((FileChannel) Files.newByteChannel(out.toPath(), EnumSet.of(StandardOpenOption.READ, StandardOpenOption.WRITE)))
                    .map(FileChannel.MapMode.READ_WRITE, 0, size);

            for (Map.Entry<String, Integer> entry : inputs.entrySet()) {
                for (int i = 0; i < entry.getValue(); ++i) {
                    sb.append(entry.getKey());
                }
            }

            //            for (String s : strs) {
            //                for (int count = 0; count < counts.get(s); ++count) {
            //                    sb.append(s);
            //                }
            //            }
            ioBuffer.put(charSet.encode(CharBuffer.wrap(sb)));
            //            end = System.currentTimeMillis();
            //            System.out.println("Process Time: " + (end - start) + " milliseconds");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        //        System.out.println("Process started...");
        File file1 = new File(args[0]);
        File file2 = new File(args[1]);
        File file3 = new File(args[2]);
        //        if (args.length == 3) {
        //            file1 = new File(args[0]);
        //            file2 = new File(args[1]);
        //            file3 = new File(args[2]);
        //        }
        //        else {
        //            System.out.println("Usage: java -jar class <file1> <file2> <file3>");
        //            return;
        //        }
        //                FastSort.makeNewFile(file1, 150000);
        //                FastSort.makeNewFile(file2, 150000);
        //                FastSort.makeNewFile(file3, 150000);
        File out1 = new File(file1.getAbsolutePath().replaceAll(".txt", ".sorted.txt"));
        File out2 = new File(file2.getAbsolutePath().replaceAll(".txt", ".sorted.txt"));
        File out3 = new File(file3.getAbsolutePath().replaceAll(".txt", ".sorted.txt"));
        //                cleanup(new File[] { out1, out2, out3 });
        long start = System.currentTimeMillis();
        try {
            Thread t1 = new Thread() {
                public void run() {
                    FastSort.runThread(file1, out1);
                }
            };
            Thread t2 = new Thread() {
                public void run() {
                    FastSort.runThread(file2, out2);
                }
            };
            Thread t3 = new Thread() {
                public void run() {
                    FastSort.runThread(file3, out3);
                }
            };
            t1.start();
            t2.start();
            t3.start();
            t1.join();
            t2.join();
            t3.join();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        long end = System.currentTimeMillis();
        System.out.println("Process Time: " + (end - start) + " milliseconds");
        System.out.println("Done.");
    }

}
