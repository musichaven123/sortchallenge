import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.Random;

public class FastSortPrimitives {
    public static void cleanup(File[] fileArray) {
        for (File file : fileArray) {
            if (file.exists()) {
                file.delete();
            }
        }
    }

    public static void makeNewFile(File file, int numberOfEntries) {
        // Write to file
        FileOutputStream fout = null;
        try {
            fout = new FileOutputStream(file.getAbsolutePath());
            Random rng = new Random();
            for (int i = 0; i < numberOfEntries; ++i) {
                fout.write(((Integer) rng.nextInt(1000000000)).toString().getBytes());
                fout.write("\n".getBytes());
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            try {
                fout.close();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void runThread(String in) {
        try (BufferedReader br = Files.newBufferedReader(Paths.get(in))) {
            File out = new File(in.replaceAll(".txt", ".sorted.txt"));
            long[] input = new long[1000000];
            int size = 0;
            // Collect inputs
            for (String line = null; (line = br.readLine()) != null; size++) {
                input[size] = Long.parseLong(line);
            }
            long[] trimput = new long[size];
            for (int i = 0; i < size; ++i) {
                trimput[i] = input[i];
            }
            
            // Sort
            Arrays.parallelSort(trimput);

            // Write output
            out.createNewFile();
            MappedByteBuffer buff = ((FileChannel) Files.newByteChannel(out.toPath(), EnumSet.of(StandardOpenOption.READ, StandardOpenOption.WRITE)))
                    .map(FileChannel.MapMode.READ_WRITE, 0, 10 * size);
            StringBuilder sb = new StringBuilder();
            for (long l : trimput) {
                buff.put(sb.append(String.valueOf(l)).append('\n').toString().getBytes());
                sb.setLength(0);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        try {
            Thread t1 = new Thread() {
                public void run() {
                    FastSortPrimitives.runThread(args[0]);
                }
            };
            Thread t2 = new Thread() {
                public void run() {
                    FastSortPrimitives.runThread(args[1]);
                }
            };
            Thread t3 = new Thread() {
                public void run() {
                    FastSortPrimitives.runThread(args[2]);
                }
            };
            t1.start();
            t2.start();
            t3.start();
            t1.join();
            t2.join();
            t3.join();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        long end = System.currentTimeMillis();
        System.out.println("Process Time: " + (end - start) + " milliseconds");
    }

}
